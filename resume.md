---
title: Resume
layout: page
subtitle: Fifteen years in IT. Ability to solve problems on desk, call or remotely.
  Extensive hands-on experience testing, installing and upgrading systems. Good communication
  skills. Experience in medium and large corporate environments.
---

# Professional Experience

[*<mark>Since 2015</mark>*] **Systems Administrator** <br>
**Northrop Grummnan**, Pittsburgh, PA <br>
Administration and deployment of Windows 2008/2012/2016 servers in Hyper-V and VMware environments. Management of Commvault, DPM and Veeam backup systems. Network troubleshooting including HP, Cisco and Juniper devices.

[*<mark>2012-2015</mark>*] **IT Specialist** <br>
**Draker**, Burlington, VT <br>
Administration and deployment of Windows 2008/R2 and Linux servers in a VMWare environment. Support of Windows 7 and Mac OSX workstations. Administration of all Active Directory infrastructure. Managing of WSUS and Remote Desktop Services. Managing and debugging of network infrastructure including HP switches, SonicWALL firewalls, EMC SANs and VPNs. Maintenance of hardware and software asset inventory. Managing user accounts and permissions, email system, antivirus system, internal information systems and phone system. Maintenance, monitoring and testing of Veeam backups.

[*<mark>2009–2011</mark>*] **Advanced Support Analyst** <br>
**IBM Canada** (Kelly Services), Montreal, QC <br>
2nd level remote support on Windows XP. Provided end users services including troubleshooting software, hardware, network connectivity, OS and application problems.

[*<mark>2005–2008</mark>*] **Incident Manager** <br> 
**EDS** (currently HP), Sao Paulo, SP <br>
Management of critical incidents per ITIL policies in a multi-platform environment. Ensured that client and leaders were aware of any critical incidents while keeping them updated on incident status. Reviewed current approach to incident resolution and determined alternative courses of actions.

# Certifications

ITIL Foundations <br>
MCSA Microsoft Certified Systems Administartor (legacy) <br>
MCSA Microsoft Certified Solution Associate (Windows Server 2008 and 2012)

# Education

[*<mark>2004–2006</mark>*] **Network and Systems Administration**, Associate <br>
**IBTA – Brazilian Institute of Advanced Technology**, Sao Paulo, SP

[*<mark>1992–1994</mark>*] **Electronics**, Technical High School <br>
**ETE Jorge Street**, Sao Caetano do Sul, SP

# Languages

English (fluent), Portuguese (native) and French (advanced)