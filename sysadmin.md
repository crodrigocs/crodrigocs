---
title: SysAdmin
layout: page
image: "/img/sys_blue.png"
subtitle: Tips for Network and Systems Administrators
---

{: .box-warning}
**Warning:** One does not simply contact IT before turning it off and on.