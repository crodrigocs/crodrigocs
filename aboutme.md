---
title: About me
layout: page
subtitle: Just a regular guy doing regular things
css: "/css/aboutme.css"
---

<p class="about-text">
<span class="fa fa-briefcase about-icon"></span>
Currently a full time sysadmin at Northrop Grumman. Previously worked for IBM, HP and Nokia in various IT functions.
</p>

<p class="about-text"> <span class="fa fa-code about-icon"></span> I started writing PowerShell scripts only a few years ago out of necessity. It became almost an obsession. Not really, but I enjoy it very much. </p>

<p class="about-text"> <span class="fa fa-map-marker about-icon"></span> Originally from <a href="https://www.lonelyplanet.com/brazil/sao-paulo">São Paulo</a>, Brazil, I moved to the beautiful yet cold Montreal in my early 30s. A few years later, I met my wife and we moved to the also beautiful state of Vermont. Several years later, I got a new job in Pittsburgh and that's where we live now.</p>
  
---

# Contact

If you just want to say hi, you can find me on [Twitter](https://twitter.com/crodrigocs "@crodrigocs"), [GitHub](https://github.com/crodrigocs "crodrigocs") or [LinkedIn](https://linkedin.com/in/crodrigocs "Rodrigo Silva"). If you are looking for something more serious, I also have a contact form. It's not that big of a deal, but it looks professional, doesn't it?

<!-- www.123formbuilder.com script begins here --><script type="text/javascript" defer src="//www.123formbuilder.com/embed/2531963.js" data-role="form" data-default-width="650px"></script>
<!-- www.123formbuilder.com script ends here -->